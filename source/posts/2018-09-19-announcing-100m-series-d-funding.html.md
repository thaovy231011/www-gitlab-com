---
title: 'Announcing $100 million in Series D round funding led by ICONiQ Capital'
Author: GitLab
author_twitter: gitlab
categories: company
image_title: '/images/blogimages/gitlab-live-sept-2018.png'
description: 'Today we announced $100M in new funding to beat nine best-in-class products with a single application.'
cta_button_text: 'Join the livestream'
cta_button_link: 'https://about.gitlab.com/webcast/whats-next-for-gitlab/'
tags: news, inside GitLab, startups
featured: yes
---

Today we are thrilled to announce our $100 million Series D funding led by ICONiQ Capital, bringing our valuation to over $1 billion and validating our position as the world’s first single application for the entire DevOps lifecycle. We’re elated that ICONiQ shares our vision of beating out nine other products with a single, best-in-class application that integrates each stage of the DevOps lifecycle. This vision enables teams to manage, plan, create, verify, package, release, configure, monitor, and secure software within a single application, making Concurrent DevOps a reality. We believe these capabilities will ultimately accelerate software development and delivery, while relieving the headache of maintaining disparate tool chains. 

> “GitLab is emerging as a leader across the entire software development ecosystem by releasing software at a pace that is unmatched by any competitor. They’re taking the broad software development market head-on by developing a application that allows organizations to churn out software at an accelerated rate with cost and time savings.” - Matthew Jacobson, General Partner at ICONiQ Capital

> “Since raising a Series C round last year, we’ve delivered on our commitment to bring a single application for the entire DevOps lifecycle to market, and as a result have been able to reach over $1 billion in valuation. With this latest funding round, we will continue to improve upon our suite by building out our management, planning, packaging, monitoring and security features for a more robust DevOps application.” 
– Sid Sijbrandij, CEO of GitLab

GitLab is purpose-built for organizations that are undergoing a digital transformation. GitLab’s focus is on supporting organizations that are aiming for faster DevOps lifecycles, cloud native architectures, and multi-cloud deployments. Some of our recent product milestones include the release of [Auto DevOps](/press/releases/2018-06-22-auto-devops-gitlab-11.html) to accelerate the DevOps lifecycle by 200 percent, a [Kubernetes integration](/2018/03/22/gitlab-10-6-released/) so clusters can be spun up from within GitLab, and [enhancements to the Web IDE](/2018/08/22/gitlab-11-2-released/) to make code changes easier for everyone. 

> “Our goal is to strive for less people managing processes and more automation within our workflow. GitLab does just that by eliminating the complicated web that tied all of our development tools together, so we now have a single, automated application that makes our team more efficient.” 
— Adam Dehnel, product architect, BI Worldwide

We began in 2014 with a mission to change all creative work from read-only to read-write, so that everyone can contribute. Since then, our all-remote company has grown from fewer than 10 to more than 350 team members in over 40 countries across the globe. And we’re not slowing down – we’re still [hiring for 77 positions](/jobs/)!

We were recently [recognized by Inc. Magazine as No. 44 out of the 5,000 fastest-growing private companies in the United States](/2018/08/16/gitlab-ranked-44-on-inc-5000-list/). We attribute our success to our open core model and our value of transparency. We have an emphasis on co-creation and a commitment to open source – [over 2,000 users and customers have contributed to GitLab’s code base](http://contributors.gitlab.com/). This philosophy helps build stronger customer relationships, which in turn result in a direct influence on feature updates to the product.

## Get involved

We owe GitLab’s existence to your enthusiasm, drive, and hard work. Without our contributors’ belief in open source software, we would not be where we are today. We need your help to make our collective vision a reality. Everyone can contribute!

We are committed to standing by our [promise to be good stewards of open source](/2016/01/11/being-a-good-open-source-steward/),
and keeping communication and collaboration amongst the community a high priority.

Visit our [contribution guide](/contributing/) to get started.